package ru.devray.day8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Fridge implements Comparable<Fridge> {
    String brand;
    String model;
    int countOfSections;

    public Fridge(String brand, String model, int countOfSections) {
        this.brand = brand;
        this.model = model;
        this.countOfSections = countOfSections;
    }

    @Override
    public String toString() {
        return "Fridge{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", countOfSections=" + countOfSections +
                '}';
    }

    @Override
    public int compareTo(Fridge another) {
        return this.brand.compareTo(another.brand); // -   0   +     -1  0  1
    }

    public static void main(String[] args) {
        List<Fridge> fridges = new ArrayList<>();
        fridges.add(new Fridge("LG", "aa1233", 2));
        fridges.add(new Fridge("Electrolux", "gg12fff33", 1));
        fridges.add(new Fridge("Bosch", "cc12fff33", 4));
        fridges.add(new Fridge("LG", "t123-f", 2));

        System.out.println(fridges);
        Collections.sort(fridges); //сортировка по умолчанию, по имени бренда
        System.out.println(fridges);

        Collections.sort(fridges, new FridgeSectionCountComparator());
        System.out.println("наш список после кастомной сортировки по количеству секций:");
        System.out.println(fridges);


        //
        Fridge strangeFridge = new Fridge("", "", 0) {
            @Override
            public String toString() {
                return "123";
            }
        };

        Fridge normalFridge = new Fridge("LG", "1233", 2);
        System.out.println(normalFridge);
        System.out.println(strangeFridge);


//        Comparator<Fridge> comparatorOfModels = new Comparator<Fridge>() {
//            @Override
//            public int compare(Fridge o1, Fridge o2) {
//                return o1.model.compareTo(o2.model);
//            }
//        };

        Collections.sort(fridges, new Comparator<Fridge>() {
            @Override
            public int compare(Fridge o1, Fridge o2) {
                return o1.model.compareTo(o2.model);
            }
        });

        Collections.sort(fridges,
                (Fridge o1, Fridge o2) -> {
                    System.out.println();
                    return o1.model.compareTo(o2.model);
                }
        );

        Collections.sort(fridges, new Comparator<Fridge>() {
            @Override
            public int compare(Fridge a1, Fridge b2) {
                return a1.model.length() - b2.model.length();
            }
        });

//        Collections.sort(fridges, comparatorOfModels);
        System.out.println(fridges);

    }

}
