package ru.devray.day8;

public class Car {

    static class Engine {
        String name;

        public Engine(String name) {
            this.name = name;
        }
    }

    class Hull {
        int weight;

        public Hull(int weight) {
            this.weight = weight;
        }
    }

    public static void main(String[] args) {
        Car.Engine engine = new Engine("JZ");

        Car car = new Car();
        Car.Hull hull = car.new Hull(12);
    }

}
