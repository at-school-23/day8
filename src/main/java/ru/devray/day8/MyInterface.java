package ru.devray.day8;

public interface MyInterface {
    void print();
    default void doStuff() {
        System.out.println("");
    }
}
