package ru.devray.day8;

import java.util.HashMap;
import java.util.Map;



public class MapDemo {
    public static void main(String[] args) {

//        List LL AL
//        Set HashSet TreeSet
        Map<String, String> autoDataBase = new HashMap<>(); //TreeMap LinkedHashMap
        autoDataBase.put("y234ав777", "Chrysler c300");
        autoDataBase.put("y234ав777", "Chrysler c300");
        autoDataBase.put("в3434щк766", "Chrysler4233");
        autoDataBase.put("в3434щк766", "Chrysler4233");
        autoDataBase.put("в3434щк761", "ВАЗ2104");
        autoDataBase.put("y234ав777", "Chrysler c301");
        autoDataBase.put(null, "Chrysler c301");
        autoDataBase.put(null, "Chrysler c302");

        String carModel = autoDataBase.get("в3434щк766");
        System.out.println(carModel);

        System.out.println(autoDataBase);

        //Map<K,V> = Set<Pair<K,V>>    Box<T>
        for (Map.Entry<String, String> entry : autoDataBase.entrySet()) {
            System.out.println(
                    String.format(
                            "Для ключа '%s' соответствует значение '%s'",
                            entry.getKey(),
                            entry.getValue()
                    )
            );
        }

    }
}
