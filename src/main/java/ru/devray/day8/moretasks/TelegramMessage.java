package ru.devray.day8.moretasks;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class TelegramMessage {

    public static final int MAX_MESSAGE_LENGTH = 1024;
    public static final int MAX_ATTACHMENTS_COUNT = 10;
    public static final int MAX_ATTACHMENT_SIZE = 768;
    public static int messageCounter = 0;

    int id;
    LocalDateTime messageSentTime;
    String text;
    String author;
    List<File> attachment;
    Map<Emojis, Integer> reactions;

    TelegramMessage quotedMessage;

    public TelegramMessage(LocalDateTime messageSentTime, String text, String author, List<File> attachment, Map<Emojis, Integer> reactions, TelegramMessage quotedMessage) {
        this.id = messageCounter++;
        this.messageSentTime = messageSentTime;
        this.text = text;
        this.author = author;
        this.attachment = attachment;
        this.reactions = reactions;
        this.quotedMessage = quotedMessage;
    }

    public TelegramMessage(int id, LocalDateTime messageSentTime, String text, String author, List<File> attachment, Map<Emojis, Integer> reactions, TelegramMessage quotedMessage) {
        this.id = id;
        this.messageSentTime = messageSentTime;
        this.text = text;
        this.author = author;
        this.attachment = attachment;
        this.reactions = reactions;
        this.quotedMessage = quotedMessage;
    }

    public TelegramMessage(String text, String author) {
        this.text = text;
        this.author = author;
    }

    public void addReaction(Emojis emojis) {

    }
}
