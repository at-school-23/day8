package ru.devray.day8.moretasks;

public enum Emojis {
    PIZZA("🍕"),
    LIKE("💖");

    String emojiSympol;

    Emojis(String emojiSympol) {
        this.emojiSympol = emojiSympol;
    }
}
