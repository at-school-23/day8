package ru.devray.day8.moretasks;

import java.util.LinkedList;

public class Main {

    /**
     * ### Задание в классе 8.0.1
     * Описываем список позиций в магазине.
     * Магазину "Четверочка" требуется хранить список всех продаваемых в магазине позиций.
     * Ассортимент мазаина часто меняется - периодически появляются новые товары, а старые - пропадают.
     * Выберите наиболее удачную стуктуру данных для этих целей.
     */

    public static void main(String[] args) {
        int[][] numbers = {
                {1,2, 3, 7}, //
                {4,2,66} //
        };

        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < 8; j++) {
                System.out.print(numbers[i][j] + ",");
            }
            System.out.println();
        }

        //LinkedList {p1, p2, p3, p4, p5, p6}
        Shop shop = new Shop(new LinkedList<>());

        TelegramMessage oldMessage = new TelegramMessage("hello!", "Bob");
        TelegramMessage newMessage = new TelegramMessage("привет Боб,рада видеть!", "Alice");
        newMessage.quotedMessage = oldMessage;
//        newMessage.setQuotedMessage("");

        oldMessage.addReaction(Emojis.LIKE);

    }


}
