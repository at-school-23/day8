package ru.devray.day8;

import java.util.*;

public class Rainbow {

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("красный");
        list.add("оранжевый");
        list.add("желтый");
        list.add("зеленый");
        list.add("голубой");
        list.add("синий");
        list.add("фиолетовый");

        Integer[] arrayOfNumbers = {1, 2, 4, 56};
        //ArrayList LinkedList
        List<Integer> listFromArray = Arrays.asList(arrayOfNumbers);
        List<Integer> listFromArray2 = Arrays.asList(new Integer[]{1, 2, 4, 56});

        List<Integer> listOfNumbers = List.of(23, 45, 10);

        List<Newspaper> newspapers = List.of(new Newspaper(), new Newspaper());
        newspapers.get(0).pageCount = 90;

        List<Integer> listOfBigNumbers = new ArrayList<>();
        listOfBigNumbers.add(45);
        listOfBigNumbers.add(-5);
        listOfBigNumbers.add(4);
        listOfBigNumbers.add(1009);
        listOfBigNumbers.add(10);

        Collections.sort(list);
        System.out.println(list);

        Collections.sort(listOfBigNumbers);
        System.out.println(listOfBigNumbers);

        Collections.reverse(listOfBigNumbers);
        System.out.println(listOfBigNumbers);

        Collections.shuffle(listOfBigNumbers);
        System.out.println(listOfBigNumbers);

        String data = "Name: Bob\n"
                + "dssdffsd: dsfsdf\n"
                + "dssdffsd: dsfsdf\n"
                + "dssdffsd: dsfsdf\n";

        String data1 = """
                first line
                second line
                and another one""";

        System.out.println(data1);



    }

}
