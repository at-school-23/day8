package ru.devray.day8;

import java.util.Comparator;

public class FridgeSectionCountComparator implements Comparator<Fridge> {

    //this Fridge
    @Override
    public int compare(Fridge o1, Fridge o2) {
        return o2.countOfSections - o1.countOfSections; //-1 0 1
    }
}
