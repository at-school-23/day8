package ru.devray.day8;

import java.util.*;

public class DemoSet {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>(); //TreeSet LinkedHashSet

        Set<Integer> integers1 = Set.of(2, 3, 5, 44, 44);
        Set<Integer> integers2 = Set.of(2, 3, 5, 44, 44);

        //JOIN SQL retainAll() addAll() removeAll()

        set.add(12); //12
        set.add(12);
        set.add(145);
        set.add(12);
        set.add(12);
        set.add(17);
        set.add(12);

        System.out.println(set.size());
        System.out.println(set);


        for (Integer i : set) {

        }
    }
}
