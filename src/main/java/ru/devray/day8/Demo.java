package ru.devray.day8;

public class Demo {
    public static void main(String[] args) {

        MyInterface m1 = new MyInterface() {
            @Override
            public void print() {
                System.out.println("abc");
                System.out.println("123");
            }

            @Override
            public void doStuff() {

            }
        };

        MyInterface m2 = () -> {
            System.out.println("abc");
            System.out.println("123");
        };
    }
}
